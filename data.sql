/*
----WARNING-----!
Please run InsertPhotosScript.java before running data.sql
*/

/*Adding ingredients to ingredients table*/
INSERT INTO Ingredients
VALUES ('I001' , 'Milk', 20.00, 15000);
INSERT INTO Ingredients
VALUES ('I002' , 'Sugar', 10.00, 1000);
INSERT INTO Ingredients
VALUES ('I003' , 'Coffee', 30.00, 10000);
INSERT INTO Ingredients
VALUES ('I004' , 'Cream', 14.00, 200);
INSERT INTO Ingredients
VALUES ('I005' , 'Chocolate Powder', 40.00, 2000);
INSERT INTO Ingredients
VALUES ('I006' , 'Milk Foam', 15.00, 5000);
INSERT INTO Ingredients
VALUES ('I007' , 'Espresso', 25.00, 5000);

/*Adding to Productingredients table*/
INSERT INTO Productingredients
VALUES ('P001' , 'I003', 200);
INSERT INTO Productingredients
VALUES ('P001', 'I002', 50);

INSERT INTO Productingredients
VALUES ('P002' , 'I001', 100);
INSERT INTO Productingredients
VALUES ('P002' , 'I005', 100);

INSERT INTO Productingredients
VALUES ('P003' , 'I007', 50);

INSERT INTO Productingredients
VALUES ('P004' , 'I007', 50);
INSERT INTO Productingredients
VALUES ('P004' , 'I006', 100);

INSERT INTO Productingredients
VALUES ('P005' , 'I007', 50);
INSERT INTO Productingredients
VALUES ('P005' , 'I006', 25);
INSERT INTO Productingredients
VALUES ('P005' , 'I001', 75);


INSERT INTO Productingredients
VALUES ('P006' , 'I007', 50);
INSERT INTO Productingredients
VALUES ('P006' , 'I006', 50);
INSERT INTO Productingredients
VALUES ('P006' , 'I001', 50);