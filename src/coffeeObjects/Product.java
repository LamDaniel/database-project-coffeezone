package coffeeObjects;

import javafx.scene.image.Image;

/**
 * Product is a class that contains all of the product information and methods to get this information
 * @author Owner
 *
 */
public class Product {
	
	//Variables
	private String id;
	private String name;
	private Image img;
	private int amountInCart;
	private double price;
	
	/**
	 * Constructor for Product
	 * @param id
	 * @param name
	 * @param price
	 * @param img
	 */
	public Product(String id,String name,double price,Image img) {
		this.amountInCart=0;
		this.id=id;
		this.name=name;
		this.price=price;
		this.img=img;
	}
	
	//Getters
	/**
	 * getId() is a method that returns the product's id
	 * @return id
	 */
	public String getId() {
		return this.id;
	}
	
	/**
	 * getName() is a method that returns the product's name
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * getPrice() is a method that returns the product's price
	 * @return price
	 */
	public double getPrice() {
		return this.price;
	}
	
	/**
	 * getImg() is a method that returns the product's image
	 * @return img
	 */
	public Image getImg() {
		return this.img;
	}
	
	/**
	 * getAmountInCart() is a method that returns the amount of this product inside the user's cart
	 * @return id
	 */
	public int getAmountInCart() {
		return this.amountInCart;
	}
	
	//Additional methods
	/**
	 * addItemToCart() is a method that adds one of the product into the user's cart
	 */
	public void addItemToCart() {
		this.amountInCart++;
	}
	
	/**
	 * removeItemFromCart() is a method that removes one amount of the product from the user's cart
	 * The amount can never go under 0
	 */
	public void removeItemFromCart() {
		if (amountInCart > 0) 
		{
			this.amountInCart--;
		}
	}
	
	/**
	 * resetAmount() is a method that resets the amount of the product from the user's cart back to 0
	 */
	public void resetAmount() {
		this.amountInCart = 0;
	}
}