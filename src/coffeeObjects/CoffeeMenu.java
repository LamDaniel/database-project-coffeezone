package coffeeObjects;

import java.util.*;
import java.io.InputStream;
import java.sql.*;
import javafx.scene.image.Image;
import jdbc.Utilities;

/**
 * CoffeeMenu is a class that contains an ArrayList with all of the database's products.
 * It can fetch the products by Name, fetch all of the products and reset the cart
 * @author Grigor Mihaylov
 */
public class CoffeeMenu {
	
	//Variables
	private ArrayList<Product> items;
	
	/**
	 * Constructor for CoffeeMenu
	 */
	public CoffeeMenu() {
		items = getProducts();
	}
	
	/**
	 * getItems() returns the ArrayList<Product> Items
	 * @return ArrayList<Product> items
	 */
	public ArrayList<Product> getItems() {
		return this.items;
	}
	
	/**
	 * getItemByName() takes a String and returns the Item that has a matching name with the parameter
	 * @param name
	 * @return Product
	 */
	public Product getItemByName(String name) {
		for (Product p : items) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}
	
	/**
	 * resetCart() resets the amount of the product in each cart back to 0
	 */
	public void resetCart() {
		for (Product p : items) {
			p.resetAmount();
		}
	}
	
	/**
	 * getProducts() makes a connection to the database and returns an ArrayList<Product> that contains the id, name, retail price and image of each product.
	 * @return ArrayList<Product> products
	 */
	private ArrayList<Product> getProducts() {
		Connection conn = Utilities.getConnection();
		ArrayList<Product> products = new ArrayList<Product>();
		
		try {
			String productsQuery = "SELECT prodid,name,retail,image FROM Products";
			PreparedStatement stmt = conn.prepareStatement(productsQuery);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				String id = rs.getString("prodid");
				String name = rs.getString("name");
				double price = rs.getDouble("retail");
				InputStream inStream = rs.getBinaryStream("image");
				Image img = new Image(inStream);
				Product newProduct = new Product(id, name, price, img);
				products.add(newProduct);
			}
		}
		catch (SQLException e) {
			System.err.println("Couldn't get products from database. (Menu array is empty)");
			e.printStackTrace();
		}
		return products;
	}
}
