package coffeeObjects;

/**
 * LoginInfo is a class that stores the username of the user who logged in and keeps tracked if he's still currently logged in
 * This class stores the methods that can set and change the username and log in status
 * @author Grigor Mihaylov
 */
public class LoginInfo {
	
	//Variables
	private boolean isLoggedIn;
	private String username;
	
	/**
	 * Constructor for LoginInfo
	 * @param isLoggedIn
	 */
	public LoginInfo(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	/**
	 * getIsLoggedIn() is a method that returns the user's login status
	 * @return isLoggedIn
	 */
	public boolean getIsLoggedIn() {
		return this.isLoggedIn;
	}
	
	/**
	 * getUsername() is a method that returns the user's username
	 * @return username
	 */
	public String getUsername() {
		return this.username;
	}
	
	/**
	 * setUsername() is a method that takes in a String and sets the user's new username with this String
	 * @param username
	 */
	public void setUsername(String username) {
		this.username= username;
	}
	
	/**
	 * setIsLoggedIn() takes a boolean and sets the logIn status depending on boolean.
	 * @param isLoggedIn
	 */
	public void setIsLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
}
