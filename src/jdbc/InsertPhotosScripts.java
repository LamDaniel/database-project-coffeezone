package jdbc;

import java.io.*;
import java.sql.*;

/**
 * InsertPhotosScripts is a class that inserts the photos of the products into the database when its main method gets run
 * @author Grigor Mihaylov
 */
public class InsertPhotosScripts {
	/**
	 * Main method
	 * @param args
	 * @throws SQLException
	 * @throws FileNotFoundException
	 */
	public static void main(String[] args) throws SQLException, FileNotFoundException{
		Connection conn= Utilities.getConnection();

		//Black Coffee
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO Products VALUES ('P001' , 'Black Coffee', 3.00, ?)");
		FileInputStream image = new FileInputStream("CoffeeZoneImages\\black.jpeg");
		stmt.setBlob(1, image);
		stmt.execute();

		//Hot Chocolate
		stmt = conn.prepareStatement("INSERT INTO Products VALUES ('P002' , 'Hot Chocolate', 3.75, ? )");
		image = new FileInputStream("CoffeeZoneImages\\chocolate2.jpg");
		stmt.setBlob(1, image);
		stmt.execute();

		//Espresso
		stmt = conn.prepareStatement("INSERT INTO Products VALUES ('P003' , 'Espresso', 2.50, ? )");
		image = new FileInputStream("CoffeeZoneImages\\espresso.jpg");
		stmt.setBlob(1, image);
		stmt.execute();

		//Macchiato
		stmt = conn.prepareStatement("INSERT INTO Products VALUES ('P004' , 'Macchiato', 3.00, ? )");
		image = new FileInputStream("CoffeeZoneImages\\m.jpg");
		stmt.setBlob(1, image);
		stmt.execute();
		
		//Latte
		stmt = conn.prepareStatement("INSERT INTO Products VALUES ('P005' , 'Latte', 4.00, ? )");
		image = new FileInputStream("CoffeeZoneImages\\lat.jpg");
		stmt.setBlob(1, image);
		stmt.execute();
		
		//Cappuccino
		stmt = conn.prepareStatement("INSERT INTO Products VALUES ('P006' , 'Cappuccino', 3.75, ? )");
		image = new FileInputStream("CoffeeZoneImages\\cap.jpeg");
		stmt.setBlob(1, image);
		stmt.execute();
		
		//Closing statement
		stmt.close();
	}
}