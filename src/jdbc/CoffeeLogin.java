package jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * CoffeeLogin is a class that contains all the methods that interacts with the database through the login tab
 * @author Daniel Lam
 */
public class CoffeeLogin 
{
	//Variables
	private Connection conn;
	private String username;
	private String password;
	
	/**
	 * Constructor of CoffeeLogin
	 * @param username
	 * @param password
	 */
	public CoffeeLogin(String username, String password)
	{
		this.conn = Utilities.getConnection();
		this.username = username;
		this.password = password;
	}
	
	/**
	 * login() is a method that checks if the username the user has put is in the database
	 * If it is,  the user's password is hashed and salted, and then compared with the password of the username in the database
	 * If they are matching, the user is logged in else the user is prompted to try again
	 * @throws SQLException
	 */
	public void login() throws SQLException 
	{
		if (checkUserName())
		{
			String salt = getSalt();
			byte[] hashedInputPassword = Utilities.hash(password, salt);
			
			byte[] hashedPassword = getPassword();
			
			if (Arrays.equals(hashedInputPassword, hashedPassword))
			{
				addNewLog();
			}
			else
			{
				throw new SQLException("Invalid password");
			}
		}
		else
		{
			throw new SQLException("Invalid username");
		}
	}
	
	/**
	 * checkUserName() is a method that checks if the user's username exists in the database
	 * @return boolean
	 * @throws SQLException
	 */
	private boolean checkUserName() throws SQLException
	{
		String usernameQuery = "SELECT username FROM CoffeeCustomers";
		PreparedStatement ps = conn.prepareStatement(usernameQuery);
		ResultSet rs = ps.executeQuery();
		while (rs.next())
		{
			if (rs.getString("username").equals(this.username))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * getSalt() is a method that returns the salt of the existing username from the database
	 * @return salt
	 * @throws SQLException
	 */
	private String getSalt() throws SQLException
	{
		String salt = null;
		String saltQuery = "SELECT salt FROM CoffeeCustomers WHERE username = ?";
		PreparedStatement saltPs = conn.prepareStatement(saltQuery);
		saltPs.setString(1, username);
		ResultSet saltRs = saltPs.executeQuery();

		while (saltRs.next())
		{
			salt = saltRs.getString("salt");
		}
		return salt;
	}
	
	/**
	 * getPassword() is a method that returns the hashed password of the existing username
	 * @return
	 * @throws SQLException
	 */
	private byte[] getPassword() throws SQLException
	{
		byte[] hashedPassword = null;
		String passwordQuery = "SELECT hashedpass FROM CoffeeCustomers WHERE username = ?";
		PreparedStatement passwordPs = conn.prepareStatement(passwordQuery);
		passwordPs.setString(1, username);
		ResultSet passwordRs = passwordPs.executeQuery();
		while (passwordRs.next())
		{
			hashedPassword = passwordRs.getBytes("hashedpass");
		}
		return hashedPassword;
	}
	
	/**
	 * addNewLog adds a new log into coffeeLog table each time the user is logged in.
	 * It records at what day and hour the user has logged in into the database
	 * @throws SQLException
	 */
	private void addNewLog() throws SQLException
	{
		String logQuery = "{call coffeeShop.logLogin(?)}";
		CallableStatement addLog = conn.prepareCall(logQuery);
		addLog.setString(1, username);
		addLog.execute();
		addLog.close();	
	}
}

