package jdbc;

import java.sql.*;

import coffeeObjects.*;

/**
 * CoffeeCheckout is a class that contains all the methods that interacts with the database through the products and checkout tab
 * @author Grigor Mihaylov
 */
public class CoffeeCheckout {
	
	//Variables
	private LoginInfo loginInfo;
	private CoffeeMenu menu;
	private Connection conn;
	private boolean isValid;
	
	/**
	 * Constructor for CoffeeCheckout
	 * @param loginInfo
	 * @param menu
	 */
	public CoffeeCheckout(LoginInfo loginInfo, CoffeeMenu menu) {
		this.loginInfo = loginInfo;
		this.menu = menu;
		this.conn = Utilities.getConnection();
		this.isValid = true;
	}

	/**
	 * addOrder is a method that takes in the user's new address (if he has one) and adds his order into the database
	 * @param newAddress
	 * @throws SQLException
	 */
	public void addOrder(String newAddress) throws SQLException  {
		try { 
			conn.setAutoCommit(false);
			
			if (newAddress.length() > 0 ) {
				updateAddress(newAddress);
			}
			
			double newDiscount = 0.0;
			String strQuery = "UPDATE CoffeeCustomers SET nextorderoff = ? WHERE username = ?";
			PreparedStatement updatingDiscount = conn.prepareStatement(strQuery);
			updatingDiscount.setDouble(1, newDiscount);
			updatingDiscount.setString(2, loginInfo.getUsername());
			updatingDiscount.executeUpdate();
			
			CallableStatement addOrder = conn.prepareCall("{CALL coffeeShop.addOrder( ? ) }");
			addOrder.setString(1, loginInfo.getUsername());
			addOrder.execute();
			addOrder.close();
			
			addProductsOfOrder();

			conn.commit();
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
			conn.rollback();
		} 
		finally {
			conn.setAutoCommit(true);
		}
	}
	
	/**
	 * addProductsOfOrder() is a method that inserts into table Orderproducts for each product inside the order
	 * @throws SQLException
	 */
	private void addProductsOfOrder() throws SQLException {
		String orderId = getMostRecentId();
		for (Product p : menu.getItems()) {
			if (p.getAmountInCart() > 0) {
				String strQuery = "INSERT INTO orderproducts VALUES ( ? , ? , ? )";
				PreparedStatement insertOrderProduct = conn.prepareStatement(strQuery);
				insertOrderProduct.setString(1, orderId);
				insertOrderProduct.setString(2, p.getId());
				insertOrderProduct.setInt(3, p.getAmountInCart());
				insertOrderProduct.execute();
			
				useIngredientsForProduct(p);
			}
		}
	}
	
	/**
	 * useIngredientsForProduct() is a method that updates the ingredient's stockamount based on how many products have been ordered.
	 * @param p
	 * @throws SQLException
	 */
	private void useIngredientsForProduct(Product p) throws SQLException {
		PreparedStatement getIngredients = conn.prepareStatement("SELECT ingredientid,quantity FROM PRODUCTINGREDIENTS WHERE PRODID = ?");
		getIngredients.setString(1, p.getId());
		ResultSet rs = getIngredients.executeQuery();
		
		while(rs.next()) {
			String ingredId = rs.getString("ingredientid");
			double quantityRequired = rs.getDouble("quantity");
			double stockDepleted = quantityRequired * p.getAmountInCart() ;
			double stockAvailable = getStock(ingredId);
			
			if (stockAvailable < stockDepleted) { //if stock gets depleted more than the available stock
				this.isValid = false;
				throw new SQLException("Not enough stock for order,");
			}
			
			String strQuery = "UPDATE Ingredients SET stockamount = stockamount - ?  WHERE ingredientid = ?";
			PreparedStatement updatingIngredients = conn.prepareStatement(strQuery);
			updatingIngredients.setDouble(1, stockDepleted);
			updatingIngredients.setString(2, ingredId);
			
			updatingIngredients.executeUpdate();
		}
	}
	
	/**
	 * getStock() is a method that returns the stockamount of the ingredient which is found through his ingredient id
	 * @param ingredId
	 * @return stock
	 * @throws SQLException
	 */
	private double getStock(String ingredId) throws SQLException {
		double stock = 0.0;
		PreparedStatement getAddress = conn.prepareStatement("SELECT stockamount FROM ingredients WHERE ingredientid = ?");
		getAddress.setString(1, ingredId);
		ResultSet rs = getAddress.executeQuery();
		
		while(rs.next()) {
			stock = rs.getDouble("stockamount");
		}
		
		return stock;
	}
	
	/**
	 * getMostRecentId() returns the id the user who made the most recent purchase id
	 * @return id
	 * @throws SQLException
	 */
	private String getMostRecentId() throws SQLException {
		CallableStatement getId = conn.prepareCall("{ ? = call coffeeShop.returnMostRecent}");
		getId.registerOutParameter(1, Types.VARCHAR);
		getId.execute();
		String id = getId.getString(1);
		getId.close();
		return id;
	}
	
	/**
	 * updateAddress() is a method that updates the user's address with its new address when invoked
	 * @param newAddress
	 * @throws SQLException
	 */
	private void updateAddress(String newAddress) throws SQLException {
		String strQuery = "UPDATE CoffeeCustomers SET deliveryaddress = ?  WHERE username = ?";
		PreparedStatement updatingAddress = conn.prepareStatement(strQuery);
		updatingAddress.setString(1, newAddress);
		updatingAddress.setString(2, loginInfo.getUsername());
		
		updatingAddress.executeUpdate();
	}
	
	/**
	 * getAddress() is a method that returns the user's address based on his username
	 * @return address
	 */
	public String getAddress() {
		String address = "";
		try {
			PreparedStatement getAddress = conn.prepareStatement("SELECT deliveryaddress FROM coffeecustomers WHERE username = ?");
			getAddress.setString(1, loginInfo.getUsername());
			ResultSet rs = getAddress.executeQuery();
			
			while(rs.next()) {
				address = rs.getString("deliveryaddress");
			}
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return address;
	}
	
	/**
	 * getDiscount() is a method that fetches the user's discount based of the number of referrals he has
	 * @return discount
	 */
	public double getDiscount() {
		double discount = 0.0;
		try {
			String query = "SELECT nextorderoff FROM COFFEECUSTOMERS WHERE username = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, loginInfo.getUsername() );
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				discount= rs.getDouble("nextorderoff");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return discount;
	}
	
	/**
	 * isValid() returns boolean value of isValid
	 * @return isValid
	 */
	public boolean isValid() {
		return this.isValid;
	}
}
