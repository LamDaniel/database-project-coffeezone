package jdbc;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * CoffeeRegister is a class that contains all the methods that interacts with the database through the register tab
 * @author Daniel Lam
 */
public class CoffeeRegister 
{
	//Variables
	private Connection conn;
	private String username;
	private String unhashedPassword;
	private String address;
	private String email;
	private String phone;
	private String referral;
	
	/**
	 * Constructor for CoffeeRegister
	 * @param username
	 * @param unhashedPassword
	 * @param address
	 * @param email
	 * @param phone
	 * @param referral
	 */
	public CoffeeRegister(String username, String unhashedPassword, String address, String email, String phone, String referral)
	{
		this.conn = Utilities.getConnection();
		this.username = username;
		this.unhashedPassword = unhashedPassword;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.referral = referral;
	}
	
	/**
	 * registerNewCustomer() takes the credentials put in by the user and creates a new record of the user into the database
	 * If the field is empty and not required, then the database saves the data of the field as null
	 * @throws SQLException
	 */
	public void registerNewCustomer(String referralInput) throws SQLException
	{	
		checkValidReferral(referralInput);
		
		String salt = getSalt();
		byte[] hashedPassword = Utilities.hash(unhashedPassword, salt);
		
		String SQL = "{call coffeeShop.addNewCustomer(?, ?, ?, ?, ?, ?, ?)}";
		CallableStatement statement = conn.prepareCall(SQL);
		statement.setString(1, username);
		statement.setBytes(2, hashedPassword);
		statement.setString(3, salt);
		statement.setString(4, address);
		statement.setString(5, email);
		statement.setString(6, phone);
		statement.setString(7, referral);
		
		statement.execute();
		
		statement.close();
	}
	
	//Additional methods
	/**
	 * getSalt() generates a random salt that gets hashed with the password
	 * @return salt
	 */
	private String getSalt()
	{
		SecureRandom random = new SecureRandom();
		String salt = new BigInteger(130, random).toString(32);
		return salt;
	}
	
	/**
	 * checkValidReferral checks if the user isn't referring himself.
	 * If he is, display error message.
	 * @param referralInput
	 * @throws SQLException
	 */
	private void checkValidReferral(String referralInput) throws SQLException
	{
		if (referralInput.equals(username))
		{
			throw new SQLException("Cannot refer yourself.");
		}
	}
}
