package jdbc;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Utilities is a class that provides methods to get the connection and random hash for the entire application when needed
 * @author Daniel Lam
 */
public class Utilities 
{
	/**
	 * getConnection() establishes a connection with the database and returns it as a Connection object
	 * @return connection
	 */
	public static Connection getConnection()
	{
		Connection connection = null;
		try
		{
			String url = "jdbc:oracle:thin:@198.168.52.73:1522/pdborad12c.dawsoncollege.qc.ca"; //local URL
			
			//Create a getConnection method that creates and returns a Connection to the database. 
			connection = DriverManager.getConnection(url, "A1937997", "borovinka13");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return connection;
	}
	
	/**
	 * hash() is a method that generates a random hashed password based on the password and salt provided and returns it
	 * @param password
	 * @param salt
	 * @return hash
	 */
	public static byte[] hash(String password, String salt)
	{
		try
		{
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
	      
			PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 1024, 256);

			SecretKey key = skf.generateSecret(spec);
	        byte[] hash = key.getEncoded();
	        return hash;
        }
		catch (NoSuchAlgorithmException | InvalidKeySpecException e) 
		{
            throw new RuntimeException(e);
        }
	}
}
