package application;

import java.sql.SQLException;

import coffeeObjects.CoffeeMenu;
import coffeeObjects.LoginInfo;
import coffeeObjects.Product;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jdbc.CoffeeCheckout;

/**
 * CheckoutTab is a class that contains the contents of the checkout tab.
 * It returns a VBox that contains the price of your products and total price.
 * @author Daniel Lam
 */
public class CheckoutTab extends Tab
{
	//Variables
	private LoginInfo loginInfo;
	private CoffeeMenu menu;
	private CoffeeCheckout checkout;
	private TextField message;
	
	/**
	 * Constructor for CheckoutTab
	 * @param loginInfo 
	 * @param menu
	 */
	public CheckoutTab(LoginInfo loginInfo, CoffeeMenu menu)
	{
		super("Checkout");
		this.loginInfo = loginInfo;
		this.menu = menu;
		this.checkout = new CoffeeCheckout(loginInfo, menu);
		this.setContent(getLayout());
 	}
	
	/**
	 * refresh() method is a method the refreshes the checkoutTab when invoked if true
	 * @param isSuccessfulOrder
	 */
	public void refresh(boolean isSuccessfulOrder) {
		this.setContent(getLayout());
		if (isSuccessfulOrder) {
			this.message.setStyle("-fx-text-fill: green;");
			this.message.setText("Order Successful! Thank you for shopping with us.");
		}
	}
	
	/**
	 * getLayout() is a method that gets the layout of the tab
	 * @return VBox
	 */
	private VBox getLayout()
	{
		/**
		 * Containers
		 */
		VBox overall = new VBox();
		overall.setSpacing(10);
		HBox titleContainer = new HBox();
		HBox cartContainer = new HBox();
		cartContainer.setSpacing(50);
		cartContainer.setMaxWidth(500);
		String css = "-fx-border-color: black;\n" +
               	"-fx-border-insets: 5;\n" +
               	"-fx-border-width: 3;\n";
		cartContainer.setStyle(css);
		HBox confirmContainer = new HBox();
		HBox messageContainer = new HBox();
		HBox addressContainer = new HBox();
		HBox updateAddressContainer = new HBox();
		
		/**
		 * Title
		 */
		String title = "Checkout \n";
		Text text = new Text(25.0, 25.0, title);
		Font font = Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 24);
		text.setFont(font);
		text.setFill(Color.BLACK);
		
		/**
		 * Message
		 */
		TextField message = new TextField();
		this.message = message;
		message.setEditable(false);
		message.setPrefWidth(550);
		
		/**
		 * Subtitles
		 */
		Font bold = Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC, 32);
		Font normalText =  Font.font("Arial", FontWeight.MEDIUM, FontPosture.REGULAR, 20);
		Text items = new Text(25.0, 25.0, "Cart Summary:");
		items.setUnderline(true);
		items.setFont(bold);
		Text total = new Text(25.0, 25.0, "Total:");
		total.setFont(bold);
		total.setUnderline(true);
		
		/**
		 * Coffee
		 */
		VBox itemsContainer = new VBox();
		VBox priceContainer = new VBox();
		itemsContainer.setSpacing(5);
		priceContainer.setSpacing(5);
		
		/**
		 * Display subtitles
		 */
		itemsContainer.getChildren().add(items);
		priceContainer.getChildren().add(total);
		
		/**
		 * Display price of each product
		 */
		double totalPrice = 0.0;
		boolean hasAtLeastOneProduct = false;
		for (Product p : menu.getItems())
		{
			if (p.getAmountInCart() > 0)
			{
				hasAtLeastOneProduct=true;
				
				Label label = new Label(p.getAmountInCart() + " x " + p.getName());
				double price = p.getPrice()*p.getAmountInCart();
				totalPrice += price;
				
				String strPrice = this.formatMoney(price);
				
				Text textPrice = new Text(25.0, 25.0,strPrice );
				label.setFont(normalText);
				textPrice.setFont(normalText);
			
				itemsContainer.getChildren().addAll(label);
				priceContainer.getChildren().addAll(textPrice);
			}
		}
		
		/**
		 * Display discount
		 * @author Grigor Mihaylov
		 */
		double discount = this.checkout.getDiscount();
		double totalDue = totalPrice - discount;
		
		if (totalDue <= 0.0) {
			totalDue = 0.0;
		}
		
		if (hasAtLeastOneProduct) {
			Label beforeDiscount = new Label("Total Before Discount");
			beforeDiscount.setFont(normalText);
			beforeDiscount.setUnderline(true);
			Label discountText = new Label("Discount");
			discountText.setFont(normalText);
			Label afterDiscount = new Label("Total Due");
			afterDiscount.setFont(bold);
			itemsContainer.getChildren().addAll(beforeDiscount, discountText, afterDiscount);
			
			Text valueBeforeDis = new Text(25.0, 25.0, this.formatMoney(totalPrice));
			valueBeforeDis.setFont(normalText);
			valueBeforeDis.setUnderline(true);
			Text valueDis = new Text(25.0, 25.0, "-" + this.formatMoney(discount));
			valueDis.setFont(normalText);
			Text valueAfterDis = new Text(25.0, 25.0, this.formatMoney(totalDue));
			valueAfterDis.setFont(bold);
			priceContainer.getChildren().addAll(valueBeforeDis, valueDis, valueAfterDis);
		}
		
		/**
		 * Delivery Address
		 */
		TextField address = new TextField("Delivering to: " + this.checkout.getAddress() );
		address.setEditable(false);
		address.setPrefWidth(500);
		
		/**
		 * Update address
		 */
		Label updateLabel = new Label("(OPTIONAL) Change address:");
		TextField updateAddress  = new TextField();
		updateAddress.setPrefWidth(250);
		
		/**
		 * Confirm purchase
		 * @author Grigor Mihaylov
		 */
		final boolean finalHasAtLeastOneProduct = hasAtLeastOneProduct;
		Button confirmPurchase = new Button("Confirm Purchase");
		confirmPurchase.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent evt)
			{
				if (loginInfo.getIsLoggedIn())
				{	
					if (finalHasAtLeastOneProduct) {
						try {
							checkout.addOrder(updateAddress.getText());
							if (checkout.isValid()) {
								menu.resetCart();
								refresh(true);
							} 
							else {
								message.setStyle("-fx-text-fill: red;");
								message.setText("We don't have enough stock for your order! Please try ordering in lesser amounts. Thank you.");
							}
						} 
						catch (SQLException e) {
							message.setStyle("-fx-text-fill: red;");
							message.setText("Order Unsuccessful! Please try again");
							e.printStackTrace();
						}
					} 
					else {
						message.setStyle("-fx-text-fill: red;");
						message.setText("No items in cart! Please add items to the cart.");
					}
				}
				else
				{
					message.setStyle("-fx-text-fill: red;");
					message.setText("You are NOT logged in! Please log in so you can access the cart.");
				}
			}
		});
		
		/**
		 * Adding contents
		 */
		titleContainer.getChildren().add(text);
		cartContainer.getChildren().addAll(itemsContainer, priceContainer);
		confirmContainer.getChildren().addAll(confirmPurchase);
		messageContainer.getChildren().addAll(message);
		addressContainer.getChildren().addAll(address);
		updateAddressContainer.getChildren().addAll(updateLabel, updateAddress);
		
		/**
		 * Final layout
		 */
		overall.getChildren().addAll(titleContainer, cartContainer, confirmContainer, messageContainer, addressContainer, updateAddressContainer);
		return overall;
	}
	//Additional methods
	/**
	 * formatMoney() takes in a double and formats the double in currency
	 * @param price
	 * @return
	 */
	private String formatMoney(double price) {
		String s = ""  + price;
		int i = s.indexOf('.');
		String str1 = s.substring(0, i);
		String str2 = s.substring(i, s.length());
		String finalStr = "$" + str1 + String.format("%-3s", str2).replace(' ', '0');
		return finalStr;
	}
}
