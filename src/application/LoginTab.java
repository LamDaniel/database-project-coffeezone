package application;

import java.sql.SQLException;

import coffeeObjects.CoffeeMenu;
import coffeeObjects.LoginInfo;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jdbc.CoffeeLogin;

/**
 * LoginTab is a class that contains the contents of the login tab.
 * It returns a VBox that asks the user to input his existing username and password.
 * @author Daniel Lam
 */
public class LoginTab extends Tab 
{
	//Variables
	private LoginInfo loginInfo;
	private CoffeeMenu menu;
	private TextField message;
	
	/**
	 * Constructor for LoginTab
	 */
	public LoginTab(LoginInfo loginInfo, CoffeeMenu menu)
	{
		super("Log In");
		this.loginInfo = loginInfo;
		this.menu = menu;
		this.setContent(getLayout());
	}

	/**
	 * getLayout() is a method that gets the layout of the tab
	 * @return VBox
	 */
	public VBox getLayout()
	{
		/**
		 * Containers
		 */
		VBox overall = new VBox();
		overall.setSpacing(2);
		HBox titleContainer = new HBox();
		HBox usernameContainer = new HBox();
		HBox passwordContainer = new HBox();
		HBox submitContainer = new HBox();
		HBox messageContainer = new HBox();
		
		/**
		 * Title
		 */
		String title = "Log In (* is required) \n";
		Text text = new Text(25.0, 25.0, title);
		Font font = Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 24);
		text.setFont(font);
		text.setFill(Color.BLACK);
		
		/**
		 * Username
		 */
		TextField usernameTf = new TextField("Username*:");
		usernameTf.setEditable(false);
		TextField usernameInput = new TextField();
		
		/**
		 * Password
		 */
		TextField passwordTf = new TextField("Password*:");
		passwordTf.setEditable(false);
		PasswordField passwordInput = new PasswordField();
		
		/**
		 * Message container
		 */
		TextField message = new TextField();
		this.message=message;
		message.setEditable(false);
		message.setPrefWidth(500);
		
		/**
		 * Submit button
		 */
		Button submitButton = new Button("Submit");
		submitButton.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent e)
			{
				CoffeeLogin coffeelogin = new CoffeeLogin(usernameInput.getText(), passwordInput.getText());
				try
				{
					coffeelogin.login();
					loginInfo.setIsLoggedIn(true);
					menu.resetCart();
					loginInfo.setUsername(usernameInput.getText());
					message.setStyle("-fx-text-fill: green;");
					message.setText("You have successfully logged in! You can now access the shop.");
					
				}
				catch (SQLException e2)
				{
					message.setStyle("-fx-text-fill: red;");
					message.setText("Invalid username or password. Try again.");
					e2.printStackTrace();
				}
			}
		});
		
		/**
		 * Adds contents into respective container
		 */
		titleContainer.getChildren().addAll(text);
		usernameContainer.getChildren().addAll(usernameTf, usernameInput);
		passwordContainer.getChildren().addAll(passwordTf, passwordInput);
		submitContainer.getChildren().addAll(submitButton);
		messageContainer.getChildren().addAll(message);
		
		/**
		 * Add everything to VBox
		 */
		overall.getChildren().addAll(titleContainer, usernameContainer, passwordContainer, submitContainer, messageContainer);
		
		return overall;
	}
	/**
	 * getMessage() is a method that returns the message textfield
	 * @return message
	 */
	public TextField getMessage() {
		return this.message;
	}
}
