package application;

import coffeeObjects.CoffeeMenu;
import coffeeObjects.LoginInfo;
import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.control.TabPane.TabClosingPolicy;

/**
 * CoffeeApplication is a class that contains the main GUI with its tabPane
 * @author Daniel Lam and Grigor Mihaylov
 */
public class CoffeeApplication extends Application { 	
    @Override
    public void start(Stage stage) {
    	/**
    	 * Variables
    	 */
		LoginInfo loginInfo = new LoginInfo(false);
    	CoffeeMenu menu = new CoffeeMenu();
		
    	/**
    	 * Container and layout
    	 */
    	Group root = new Group();
    	
    	/**
    	 * Scene is associated with container, dimensions
    	 */
    	Color backgroundColor = Color.web("#d4fffe", 1.0);
    	Scene scene = new Scene(root, 1300, 950); 
    	scene.setFill(backgroundColor);
    	
    	/**
    	 * TabPane
    	 */
		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabPane.setMinWidth(5000);
		tabPane.setTabMinWidth(100);

		/**
		 * Tabs
		 */
		RegisterTab registerTab = new RegisterTab();
		LoginTab loginTab = new LoginTab(loginInfo, menu);
		ProductsTab productsTab = new ProductsTab(loginInfo, menu);
		CheckoutTab checkoutTab = new CheckoutTab(loginInfo, menu);
		
		/**
		 * EventListeners to refresh checkoutTab
		 */
		TextField productSMessage = productsTab.getMessage();
		productSMessage.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				checkoutTab.refresh(false);
			}
		});
		
		TextField loginSMessage = loginTab.getMessage();
		loginSMessage.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				checkoutTab.refresh(false);
			}
		});
		
		/**
		 * Add tabs to tabPane
		 */
		tabPane.getTabs().addAll(registerTab, loginTab, productsTab, checkoutTab);

		/**
		 * Display GUI
		 */
		root.getChildren().addAll(tabPane);
		
		/**
		 * associate scene to stage and show
		 */
		stage.setTitle("CoffeeZone"); 
		stage.setScene(scene); 
		stage.show(); 
	} 
    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(args);
    }  
} 
