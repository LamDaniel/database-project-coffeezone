package application;

import coffeeObjects.LoginInfo;

import java.util.ArrayList;

import coffeeObjects.CoffeeMenu;
import coffeeObjects.Product;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * ProductsTab is a class that contains the contents of the products tab.
 * It returns a VBox that displays all the products with their information
 * @author Daniel Lam
 */
public class ProductsTab extends Tab
{
	//Variables
	private LoginInfo loginInfo;
	private CoffeeMenu menu;
	private TextField message;
	
	/**
	 * Constructor for ProductsTab
	 * @param loginInfo
	 */
	public ProductsTab(LoginInfo loginInfo, CoffeeMenu menu)
	{
		super("Products");
		this.loginInfo = loginInfo;
		this.menu = menu;
		this.setContent(getLayout());
	}

	private VBox getLayout()
	{
		/**
		 * Containers
		 */
		VBox overall = new VBox();
		overall.setSpacing(25);
		HBox titleContainer = new HBox();
		HBox firstRow = new HBox();
		firstRow.setSpacing(20);
		HBox secondRow = new HBox();
		secondRow.setSpacing(20);
		HBox messageContainer = new HBox();
		
		/**
		 * Message
		 */
		TextField message = new TextField();
		this.message = message;
		message.setEditable(false);
		message.setPrefWidth(500);
		
		/**
		 * Title
		 */
		String title = "Products";
		Text text = new Text(25.0, 25.0, title);
		Font font = Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 24);
		Font bold = Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC, 12);
		text.setFont(font);
		text.setFill(Color.BLACK);
		
		/**
		 * Setting up the layout for each product
		 * @author Daniel Lam and Grigor Mihaylov
		 */
		ArrayList<VBox> vboxArr = new ArrayList<VBox>();
		for (Product p : menu.getItems())
		{
			VBox container = new VBox();
			container.setSpacing(5);
			String css = "-fx-border-color: black;\n" +
	                   	"-fx-border-insets: 5;\n" +
	                   	"-fx-border-width: 3;\n";
			container.setStyle(css);
			Label label = new Label(p.getName());
			label.setFont(bold);
			ImageView img = new ImageView(p.getImg());
			String strPrice = String.format("%-4s", "" + p.getPrice()).replace(' ', '0');
			Text price = new Text(25.0, 25.0, "Price: $" + strPrice);
			
			Button addToCart = new Button("Add to cart");
			addToCart.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e)
				{
					if (loginInfo.getIsLoggedIn())
					{
						p.addItemToCart();
						message.setStyle("-fx-text-fill: green;");
						message.setText("Added 1 " + p.getName() + " to cart! You now have " + p.getAmountInCart() + " " + p.getName() + " in your cart.");
					}
					else
					{
						message.setStyle("-fx-text-fill: red;");
						message.setText("You are NOT logged in! Please log in before adding to cart.");
					}
				}
			});
			
			Button removeFromCart = new Button("Remove From Cart");
			removeFromCart.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e)
				{
					if (loginInfo.getIsLoggedIn())
					{
						p.removeItemFromCart();
						message.setStyle("-fx-text-fill: green;");
						message.setText("Removed 1 " + p.getName() + " from cart! You now have " + p.getAmountInCart() + " " + p.getName() + " in your cart.");
					}
					else
					{
						message.setStyle("-fx-text-fill: red;");
						message.setText("You are NOT logged in! Please log in before removing from cart.");
					}
				}
			});
			container.getChildren().addAll(img, label, price, addToCart, removeFromCart);
			
			vboxArr.add(container);
		}
		
		/**
		 * Adding contents into respective containers
		 */
		titleContainer.getChildren().add(text);
		firstRow.getChildren().addAll(vboxArr.get(0), vboxArr.get(1), vboxArr.get(2));
		secondRow.getChildren().addAll(vboxArr.get(3), vboxArr.get(4), vboxArr.get(5));
		messageContainer.getChildren().addAll(message);
		
		/**
		 * Set everything to VBox
		 */
		overall.getChildren().addAll(titleContainer, firstRow, secondRow, messageContainer);
		
		return overall;
	}
	
	/**
	 * getMessage() is a method that returns the message textfield
	 * @return message
	 */
	public TextField getMessage() {
		return this.message;
	}
}
