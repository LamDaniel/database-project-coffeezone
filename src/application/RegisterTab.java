package application;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jdbc.CoffeeRegister;

/**
 * RegisterTab is a class that contains the contents of the register tab.
 * It returns a VBox that asks the user to input his credentials.
 * @author Daniel Lam
 */
public class RegisterTab extends Tab
{
	/**
	 * Constructor for RegisterTab
	 */
	public RegisterTab()
	{
		super("Register");
		this.setContent(getLayout());
	}
	/**
	 * getLayout() is a method that gets the layout of the tab
	 * @return VBox
	 */
	private VBox getLayout()
	{
		/**
		 * Containers
		 */
		VBox overall = new VBox();
		overall.setSpacing(2);
		HBox titleContainer = new HBox();
		HBox usernameContainer = new HBox();
		HBox passwordContainer = new HBox();
		HBox addressContainer = new HBox();
		HBox emailContainer = new HBox();
		HBox phoneContainer = new HBox();
		HBox referralContainer = new HBox();
		HBox submitContainer = new HBox();
		HBox messageContainer = new HBox();
		
		/**
		 * Title
		 */
		String title = "Register (* is required) \n";
		Text text = new Text(25.0, 25.0, title);
		Font font = Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 24);
		text.setFont(font);
		text.setFill(Color.BLACK);
		
		/**
		 * Username
		 */
		TextField usernameTf = new TextField("Username*:");
		usernameTf.setEditable(false);
		TextField usernameInput = new TextField();
		
		/**
		 * Password
		 */
		TextField passwordTf = new TextField("Password*:");
		passwordTf.setEditable(false);
		PasswordField passwordInput = new PasswordField();
		
		/**
		 * Address
		 */
		TextField addressTf = new TextField("Address*:");
		addressTf.setEditable(false);
		TextField addressInput = new TextField();
		
		/**
		 * Email
		 */
		TextField emailTf = new TextField("Email:");
		emailTf.setEditable(false);
		TextField emailInput = new TextField();
		
		/**
		 * Phone Number
		 */
		TextField phoneTf = new TextField("Phone Number:");
		phoneTf.setEditable(false);
		TextField phoneInput = new TextField();
		
		/**
		 * Referral
		 */
		TextField referralTf = new TextField("Username of person who referred you: ");
		referralTf.setPrefWidth(250);
		referralTf.setEditable(false);
		TextField referralInput = new TextField();
		
		/**
		 * Message
		 */
		TextField message = new TextField();
		message.setEditable(false);
		message.setPrefWidth(500);
		
		/**
		 * Array to add all of the textfields
		 */
		TextField[] textfieldArr = {usernameInput, passwordInput, addressInput, emailInput, phoneInput, referralInput};
		
		/**
		 * Submit Button
		 */
		Button submitButton = new Button("Submit");
		
		/**
		 * Event Handlers
		 */
		submitButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e)
			{
				CoffeeRegister register = new CoffeeRegister(usernameInput.getText(), passwordInput.getText(), addressInput.getText(), emailInput.getText(), phoneInput.getText(), referralInput.getText());
				try 
				{
					register.registerNewCustomer(referralInput.getText());
					resetTextFields(textfieldArr);
					message.setStyle("-fx-text-fill: green;");
					message.setText("Register successful! Please log in through the Log In tab.");
				} 
				catch (SQLException e2) 
				{
					resetTextFields(textfieldArr);
					message.setStyle("-fx-text-fill: red;");
					message.setText("Invalid register information. Please try again.");
					e2.printStackTrace();
				}
			}
		});
		
		/**
		 * Adding all of the contents to its respective containers
		 */
		titleContainer.getChildren().add(text);
		usernameContainer.getChildren().addAll(usernameTf, usernameInput);
		passwordContainer.getChildren().addAll(passwordTf, passwordInput);
		addressContainer.getChildren().addAll(addressTf, addressInput);
		emailContainer.getChildren().addAll(emailTf, emailInput);
		phoneContainer.getChildren().addAll(phoneTf, phoneInput);
		referralContainer.getChildren().addAll(referralTf, referralInput);
		submitContainer.getChildren().add(submitButton);
		messageContainer.getChildren().add(message);
		
		/**
		 * Adding all of the containers to the main VBox container
		 */
		overall.getChildren().addAll(titleContainer,usernameContainer,passwordContainer, addressContainer, emailContainer, phoneContainer, referralContainer, submitContainer, messageContainer);
		return overall;
	}
	//Additional method
	/**
	 * resetTextFields is a method that takes in an array of textFields and sets its text back to blank.
	 * @param tfArr
	 */
	private void resetTextFields(TextField[] tfArr)
	{
		for (TextField tf : tfArr)
		{
			tf.setText("");
		}
	}
}
