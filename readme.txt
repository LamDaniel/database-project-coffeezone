Welcome to CoffeeZone!
by Daniel Lam 1932789 and Grigor Mihaylov 1937997

Our App works with last year's database (pdborad12c), so you don't need to run any scripts prior to launching the app.

INSTRUCTIONS:

To launch the app, run CoffeeApplication.java in application package.

First, register in the Register Tab and then log in from the LogIn Tab. 
You can add items to your cart from the Products Tab and finalize your order in the Checkout Tab.
You can even change your address before confirming the checkout.


IN CASE THE DATABASE IS DOWN:

I. In Utilities.java in the jdbc package, you need to change the url, username and password to match your local database's (or some other database) credentials.

II. Run all the scripts on your local database(or some other database)

	Order:
	1. schema.sql in SQL Developer
	2. InsertPhotosScript.java in the jdbc package (you were provided with the photos folder in the zip file)
	3. data.sql in SQL Developer
	4. procs.sql in SQL Developer

After that you should be able to run CoffeeApplication.java 

ENJOY!
~Daniel Lam and Grigor Mihaylov