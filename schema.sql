DROP TABLE CoffeeCustomers CASCADE CONSTRAINTS;

CREATE TABLE CoffeeCustomers (
custid VARCHAR2(4) ,
username VARCHAR2(30) NOT NULL UNIQUE,
hashedPass RAW (200) NOT NULL,
salt VARCHAR2(32) NOT NULL,
deliveryaddress VARCHAR2(30) NOT NULL,
email VARCHAR2(30),
phone VARCHAR2(12),
referral VARCHAR2(4),
nextOrderOff NUMBER(5,2),
    CONSTRAINT customer_pk PRIMARY KEY(custid),
    CONSTRAINT referral_fk FOREIGN KEY (referral)
    REFERENCES CoffeeCustomers(custid)
);

DROP TABLE CoffeeLog CASCADE CONSTRAINTS;

CREATE TABLE CoffeeLog (
    custid VARCHAR2(4),
    dateaccessed TIMESTAMP,
        CONSTRAINT customer_id_fk FOREIGN KEY (custid)
         REFERENCES CoffeeCustomers(custid)
);

DROP TABLE Ingredients CASCADE CONSTRAINTS;

CREATE TABLE Ingredients (
ingredientid VARCHAR2(4),
ingredientname VARCHAR2(30) NOT NULL UNIQUE,
restockprice NUMBER(5,2) NOT NULL,
stockamount NUMBER(6) NOT NULL,
    CONSTRAINT ingredient_id PRIMARY KEY(ingredientid)
);

DROP TABLE Products CASCADE CONSTRAINTS;

CREATE TABLE Products (
prodid VARCHAR2(4) ,
name VARCHAR2(30) NOT NULL UNIQUE,
retail NUMBER(5,2) NOT NULL,
image BLOB,
    CONSTRAINT product_id PRIMARY KEY(prodid)
);

DROP TABLE Productingredients CASCADE CONSTRAINTS;

CREATE TABLE Productingredients (
prodid VARCHAR2(4),
ingredientid VARCHAR2(4),
quantity NUMBER (5),
    CONSTRAINT product_fk FOREIGN KEY(prodid) REFERENCES Products(prodid),
    CONSTRAINT ingredient_fk FOREIGN KEY(ingredientid) REFERENCES Ingredients(ingredientid)
);

DROP TABLE CoffeeOrders CASCADE CONSTRAINTS;

CREATE TABLE CoffeeOrders (
orderid VARCHAR2(4),
deliveryaddress VARCHAR2(30) NOT NULL,
orderdate DATE NOT NULL,
shipdate DATE,
    CONSTRAINT coffee_order_id PRIMARY KEY(orderid)
);

DROP TABLE Orderproducts CASCADE CONSTRAINTS;

CREATE TABLE Orderproducts (
orderid VARCHAR2(4),
prodid VARCHAR2(4),
amount NUMBER(2),
    CONSTRAINT coffee_order_fk FOREIGN KEY(orderid) REFERENCES CoffeeOrders(orderid),
    CONSTRAINT product_id_fk FOREIGN KEY(prodid) REFERENCES Products(prodid)
);
