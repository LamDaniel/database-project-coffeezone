/*PROCS.SQL*/

/*SEQUENCES*/

/*Customer sequence*/
DROP SEQUENCE customerCounter;
CREATE SEQUENCE customerCounter
        MINVALUE 001
        MAXVALUE 999
        START WITH 001
        INCREMENT BY 1
        CACHE 20;
        
/*Order Sequence*/
DROP SEQUENCE orderCounter;
CREATE SEQUENCE orderCounter
        MINVALUE 001
        MAXVALUE 999
        START WITH 001
        INCREMENT BY 1
        CACHE 20;

/*TRIGGERS*/

/*addDiscount trigger*/
DROP TRIGGER addDiscount;
CREATE TRIGGER addDiscount BEFORE INSERT
ON CoffeeCustomers
FOR EACH ROW
BEGIN 
    IF :NEW.referral IS NOT NULL  THEN
        UPDATE coffeecustomers
        SET nextOrderOff = nextOrderOff +  5
        WHERE custid=:NEW.referral;
    END IF;
END;
/

/*PACKAGE*/
CREATE OR REPLACE PACKAGE coffeeShop AS
        mostRecentOrderId coffeeorders.orderid%TYPE;
        FUNCTION returnMostRecent
        RETURN coffeeorders.orderid%TYPE;
        PROCEDURE addNewCustomer(
            username IN CoffeeCustomers.username%TYPE, 
            hashedPass IN CoffeeCustomers.hashedPass%TYPE, 
            salt IN CoffeeCustomers.salt%TYPE,
            address IN CoffeeCustomers.deliveryaddress%TYPE,
            emailInput IN CoffeeCustomers.email%TYPE,
            phoneInput IN CoffeeCustomers.phone%TYPE,
            referralInput IN CoffeeCustomers.referral%TYPE
        );
        PROCEDURE logLogin(
            loginUsername IN CoffeeCustomers.username%TYPE
        );
        PROCEDURE addOrder(
            loginUsername IN CoffeeCustomers.username%TYPE
        );
END coffeeShop;
/

/*PACKAGE BODY*/
CREATE OR REPLACE PACKAGE BODY coffeeShop AS
        FUNCTION returnMostRecent
        RETURN coffeeorders.orderid%TYPE
        AS
        BEGIN
            RETURN mostrecentorderid;
        END;
        PROCEDURE addOrder(
            loginUsername IN CoffeeCustomers.username%TYPE) IS
        todayDate DATE;
        address coffeecustomers.deliveryaddress%TYPE;
        newId CoffeeOrders.orderid%TYPE;
        BEGIN
            todayDate := CURRENT_DATE;
            newId := 'O' || LPAD(orderCounter.NEXTVAL,3,'0') ;
            SELECT deliveryaddress INTO address FROM coffeecustomers
            WHERE coffeecustomers.username =  loginUsername;
            
            coffeeshop.mostrecentorderid :=newid;
            
            INSERT INTO CoffeeOrders
            VALUES (newId, address,todayDate,NULL);
        END;
        PROCEDURE logLogin(
           loginUsername IN CoffeeCustomers.username%TYPE) IS
        todayDate TIMESTAMP;
        loginId coffeeCustomers.custid%TYPE;
        BEGIN
            todayDate := CURRENT_DATE;
            SELECT custid INTO loginId FROM coffeecustomers
            WHERE coffeecustomers.username =  loginUsername;
            INSERT INTO CoffeeLog
            VALUES (loginId, todayDate);
        END;
        PROCEDURE addNewCustomer(
                username IN CoffeeCustomers.username%TYPE, 
                hashedPass IN CoffeeCustomers.hashedPass%TYPE, 
                salt IN CoffeeCustomers.salt%TYPE,
                address IN CoffeeCustomers.deliveryaddress%TYPE,
                emailInput IN CoffeeCustomers.email%TYPE,
                phoneInput IN CoffeeCustomers.phone%TYPE,
                referralInput IN CoffeeCustomers.referral%TYPE) IS
            newEmail coffeecustomers.email%TYPE;
            newPhone coffeeCustomers.phone%TYPE ;
            newReferral coffeeCustomers.referral%TYPE ;
            newId coffeeCustomers.custid%TYPE;
            nmbOfReferral INT;
            BEGIN
                
                IF emailinput = '' THEN
                    newEmail := NULL;
                ELSE
                    newEmail :=emailInput;
                END IF;
                
                IF phoneinput = '' THEN
                    newPhone := NULL;
                ELSE
                    newPhone :=phoneInput;
                END IF;
                
                BEGIN 
                    SELECT custid INTO newreferral FROM coffeecustomers
                    WHERE coffeecustomers.username = referralInput;
                    
                    SELECT COUNT(*) INTO nmbOfReferral FROM coffeecustomers 
                    WHERE referral = newreferral;
                    
                    IF nmbofreferral +1 > 3 THEN
                       newreferral := NULL;
                    END IF;  
                EXCEPTION
                WHEN no_data_found THEN
                    newreferral := NULL;
                END;
                
                newId := 'C' || LPAD(customerCounter.NEXTVAL,3,'0') ;
                
                INSERT INTO coffeecustomers
                VALUES (newId ,username, hashedPass,salt, address, newEmail, newPhone, newReferral, 0);
        END;
END coffeeShop;
/

